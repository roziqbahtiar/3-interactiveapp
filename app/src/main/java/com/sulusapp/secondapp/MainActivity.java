package com.sulusapp.secondapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int quantity = 0;
    Button increment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        increment = (Button) findViewById(R.id.increment);
        increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantity = quantity + 1;
                Log.i("Nilai_quantity", String.valueOf(quantity));

                display(quantity);
            }
        });
    }

    public void submitOrder(View view){
        display (quantity);
        displayPrice(quantity*5);
    }

    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

    private void displayPrice (int number) {
        TextView priceTextView = (TextView) findViewById(R.id.price_text_view);
        priceTextView.setText("$" + number);
    }

    public void incrementButton(View view) {
        quantity += 1;
        display(quantity);
    }

    public void decrementButton(View view) {
        if (quantity >0) {
            quantity = quantity - 1;
            display(quantity);
        }
    }
}
