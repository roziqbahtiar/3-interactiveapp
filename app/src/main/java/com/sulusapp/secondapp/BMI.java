package com.sulusapp.secondapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BMI extends AppCompatActivity {

    EditText beratBadan, tinggiBadan;
    TextView tvIndeksMasaTubuh;
    Button hitungIMT;

    float tBadan,bBadan;
    float hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);

        beratBadan = (EditText) findViewById(R.id.beratbdn);
        tinggiBadan = (EditText) findViewById(R.id.tinggibdn);
        tvIndeksMasaTubuh = (TextView) findViewById(R.id.tvIMT);
        hitungIMT = (Button) findViewById(R.id.hitungIMT);


        hitungIMT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tBadan = Float.valueOf(tinggiBadan.getText().toString());
                bBadan = Float.valueOf(beratBadan.getText().toString());

                hasil = hitungIMT(bBadan, tBadan);
                tvIndeksMasaTubuh.setText(String.valueOf(hasil)+" Kg/m2");
            }
        });


    }

    public float hitungIMT(float bBadan, float tBadan){
        return bBadan/tBadan;
    }
}
